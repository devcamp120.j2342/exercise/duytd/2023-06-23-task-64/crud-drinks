package com.devcamp.apidrinks.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.apidrinks.model.CMenu;
import com.devcamp.apidrinks.repository.MenuRepository;

@Service
public class MenuService {
    @Autowired
    private MenuRepository menuReposytory;

    public List<CMenu> getAllMenuSV() {
        List<CMenu> menuList = menuReposytory.findAll();
        return menuList;
    }

    public CMenu createMenu(CMenu menuCilent) {
        CMenu menuData = new CMenu();
        menuData.setDonGia(menuCilent.getDonGia());
        menuData.setDuongKinh(menuCilent.getDuongKinh());
        menuData.setSalad(menuCilent.getSalad());
        menuData.setSize(menuCilent.getSize());
        menuData.setSoLuongNuocNgot(menuCilent.getSoLuongNuocNgot());
        menuData.setSuon(menuCilent.getSuon());
        CMenu menuSave = menuReposytory.save(menuData);
        return menuSave;
    }

    public CMenu updateMenuSV(long id, CMenu updateMenu) {
        Optional <CMenu> menuData = menuReposytory.findById(id);
        if (menuData.isPresent()){
            CMenu menu = menuData.get();
            menu.setDonGia(updateMenu.getDonGia());
            menu.setDuongKinh(updateMenu.getDuongKinh());
            menu.setSalad(updateMenu.getSalad());
            menu.setSize(updateMenu.getSize());
            menu.setSoLuongNuocNgot(updateMenu.getSoLuongNuocNgot());
            menu.setSuon(updateMenu.getSuon());
            CMenu saveMenu = menuReposytory.save(menu);
            return saveMenu;
        }else {
            return null;
        }
    }

    public List<CMenu> daleteMenuSV(long id) {
        Optional<CMenu> menu = menuReposytory.findById(id);
        if (menu.isPresent()){
            menuReposytory.deleteById(id);
            return null;
        }else {
            return null;
        }
    }
}
