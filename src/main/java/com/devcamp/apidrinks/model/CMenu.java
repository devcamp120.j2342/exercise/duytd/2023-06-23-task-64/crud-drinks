package com.devcamp.apidrinks.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name = "cmenu")
public class CMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="kich_co")
    private char size;

    @Column(name="duong_kinh")
    private int duongKinh;

    @Column(name="suon")
    private int suon;

    @Column(name="salad")
    private int salad;

    @Column(name="so_luong_nuoc_ngot")
    private int soLuongNuocNgot;

    @NotNull(message = "nhập đơn giá")
    @Range(min=150000, max=250000, message = "giá trị từ 150000 đến 250000")
    @Column(name="don_gia")
    private int donGia;

    public CMenu() {
    }

    public CMenu(char size, int duongKinh, int suon, int salad, int soLuongNuocNgot, int donGia) {
        this.size = size;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.soLuongNuocNgot = soLuongNuocNgot;
        this.donGia = donGia;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public char getSize() {
        return size;
    }

    public void setSize(char size) {
        this.size = size;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getSoLuongNuocNgot() {
        return soLuongNuocNgot;
    }

    public void setSoLuongNuocNgot(int soLuongNuocNgot) {
        this.soLuongNuocNgot = soLuongNuocNgot;
    }

    public int getDonGia() {
        return donGia;
    }

    public void setDonGia(int donGia) {
        this.donGia = donGia;
    }
}
