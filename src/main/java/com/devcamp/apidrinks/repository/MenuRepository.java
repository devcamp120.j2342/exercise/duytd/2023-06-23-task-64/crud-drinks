package com.devcamp.apidrinks.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.apidrinks.model.CMenu;

public interface MenuRepository extends JpaRepository<CMenu, Long>{
    
}
