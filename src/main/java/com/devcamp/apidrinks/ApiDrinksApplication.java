package com.devcamp.apidrinks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiDrinksApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiDrinksApplication.class, args);
	}

}
